v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 2580 -660 2580 -640 { lab=GND}
N 2580 -750 2580 -720 { lab=VPWR}
N 2650 -660 2650 -640 { lab=GND}
N 2650 -750 2650 -720 { lab=VGND}
N 2720 -660 2720 -640 { lab=GND}
N 2720 -750 2720 -720 { lab=VC}
N 2790 -660 2790 -640 { lab=GND}
N 2790 -750 2790 -720 { lab=Ini_con}
N 2260 -660 2260 -630 { lab=GND}
N 1990 -660 1990 -640 { lab=GND}
N 1990 -750 1990 -720 { lab=Ini_en}
N 2260 -750 2260 -720 { lab=ref_clk}
C {devices/vsource.sym} 2580 -690 0 0 {name=V2 value=1.8}
C {devices/lab_pin.sym} 2580 -750 1 0 {name=l6 sig_type=std_logic lab=VPWR}
C {devices/gnd.sym} 2580 -640 0 0 {name=l7 lab=GND}
C {devices/vsource.sym} 2650 -690 0 0 {name=V1 value=0}
C {devices/lab_pin.sym} 2650 -750 1 0 {name=l11 sig_type=std_logic lab=VGND}
C {devices/gnd.sym} 2650 -640 0 0 {name=l12 lab=GND}
C {devices/vsource.sym} 2720 -690 0 0 {name=V6 value=0.54}
C {devices/lab_pin.sym} 2720 -750 1 0 {name=l10 sig_type=std_logic lab=VC}
C {devices/gnd.sym} 2720 -640 0 0 {name=l18 lab=GND}
C {devices/vsource.sym} 2790 -690 0 0 {name=V8 value=0.33}
C {devices/lab_pin.sym} 2790 -750 1 0 {name=l31 sig_type=std_logic lab=Ini_con}
C {devices/gnd.sym} 2790 -640 0 0 {name=l32 lab=GND}
C {devices/vsource.sym} 2260 -690 0 0 {name=V12 value="PULSE (0 1.8 2.4NS 2PS 2PS 20NS 40NS)"}
C {devices/gnd.sym} 2260 -630 0 0 {name=l44 lab=GND}
C {devices/vsource.sym} 1990 -690 0 0 {name=V13 value="PULSE (0 1.8 0 2PS 2PS 43NS 160000NS)"}
C {devices/lab_pin.sym} 1990 -750 1 0 {name=l67 sig_type=std_logic lab=Ini_en}
C {devices/gnd.sym} 1990 -640 0 0 {name=l68 lab=GND}
C {devices/lab_pin.sym} 2260 -750 1 0 {name=l69 sig_type=std_logic lab=ref_clk}
C {devices/code.sym} 1940 -360 0 0 {name=TT_MODELS
only_toplevel=false
format="tcleval( @value )"
value="
.lib \\\\$::SKYWATER_MODELS\\\\/sky130.lib.spice tt *Use this statement to import primitives
.include \\\\$::SKYWATER_STDCELLS\\\\/sky130_fd_sc_hd.spice *Use this statement to import STD Cells
"
}
C {devices/title.sym} 2070 -20 0 0 {name=l1 author="Parker Hardy"}
C {devices/code_shown.sym} 2970 -720 0 0 {name=NGSPICE only_toplevel=false 
value="
.title Chaotic Oscillator Testbench V1
parameter sweep
.control
*.options CONVSTEP=10n
.options INTERP
set wr_singlescale
*set wr_vecnames
set appendwrite

let start_VC = 0.500
let stop_VC= 0.505
let delta_VC=0.005
let VC_act= start_VC

*loop
while VC_act le stop_VC
alter V6 VC_act
tran 0.1n 160n
*tran 20n 160000n
plot VOUT_2

let VC_act = VC_act + delta_VC

*wrdata T05_G5_non_ideal_clk.txt VC, VOUT_2
*wrdata T05_G8_non_ideal_clk_interp_newSW.txt VC, VOUT_1, VOUT_2

end

save all
.endc
.end
"}
C {umBMOSC/Chaotic_Oscillator/Chaotic_osc.sym} 2480 -390 0 0 {name=x1 VGND=VGND VPWR=VPWR}
C {devices/lab_pin.sym} 2630 -420 0 1 {name=p1 lab=map1_OUT}
C {devices/lab_pin.sym} 2330 -420 0 0 {name=p2 lab=Ini_con}
C {devices/lab_pin.sym} 2330 -400 0 0 {name=p3 lab=Ini_en}
C {devices/lab_pin.sym} 2330 -380 0 0 {name=p4 lab=VC}
C {devices/lab_pin.sym} 2630 -400 0 1 {name=p5 lab=VOUT_2}
C {devices/lab_pin.sym} 2630 -380 0 1 {name=p6 lab=VOUT_1}
C {devices/lab_pin.sym} 2630 -360 0 1 {name=p7 lab=map2_OUT}
C {devices/lab_pin.sym} 2330 -360 0 0 {name=p8 lab=ref_clk}
