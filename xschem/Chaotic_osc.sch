v {xschem version=3.0.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 990 -2090 1100 -2090 { lab=VOUT_2}
N 810 -2090 820 -2090 { lab=Ini_con}
N 810 -2070 820 -2070 { lab=Ini_en}
N 1410 -1900 1540 -1900 { lab=map2_OUT}
N 1350 -2160 1370 -2160 { lab=map1_OUT}
N 1350 -2160 1350 -2090 { lab=map1_OUT}
N 1330 -1900 1410 -1900 { lab=map2_OUT}
N 1250 -2090 1390 -2090 { lab=map1_OUT}
N 1670 -2450 1690 -2450 { lab=#net1}
N 1540 -2430 1550 -2430 { lab=Ini_en_inv}
N 1770 -2450 1790 -2450 { lab=en_1}
N 1540 -2470 1550 -2470 { lab=clk1ADV}
N 1670 -2350 1690 -2350 { lab=#net2}
N 1540 -2330 1550 -2330 { lab=Ini_en_inv}
N 1770 -2350 1790 -2350 { lab=en_2}
N 1540 -2370 1550 -2370 { lab=clk2ADV}
N 1390 -2090 1530 -2090 { lab=map1_OUT}
N 1030 -2090 1030 -1920 { lab=VOUT_2}
N 1510 -2070 1530 -2070 { lab=en_1}
N 1530 -2260 1560 -2260 { lab=Ini_en}
N 1640 -2260 1780 -2260 { lab=Ini_en_inv}
N 1030 -1900 1100 -1900 { lab=VOUT_2}
N 1030 -1920 1030 -1900 { lab=VOUT_2}
N 1270 -1900 1330 -1900 { lab=map2_OUT}
N 1700 -2090 1740 -2090 { lab=VOUT_1}
N 1740 -2090 1740 -1900 { lab=VOUT_1}
N 1690 -1900 1740 -1900 { lab=VOUT_1}
N 1740 -1990 1780 -1990 { lab=VOUT_1}
N 1030 -2000 1060 -2000 { lab=VOUT_2}
N 1360 -1970 1380 -1970 { lab=map2_OUT}
N 1360 -1970 1360 -1900 { lab=map2_OUT}
N 1070 -2070 1100 -2070 { lab=VC}
N 1070 -2070 1070 -2030 { lab=VC}
N 850 -2030 1070 -2030 { lab=VC}
N 850 -2030 850 -2010 { lab=VC}
N 810 -2010 850 -2010 { lab=VC}
N 1070 -2030 1700 -2030 { lab=VC}
N 1700 -2030 1720 -2030 { lab=VC}
N 1720 -2030 1720 -1880 { lab=VC}
N 1690 -1880 1720 -1880 { lab=VC}
C {devices/lab_pin.sym} 1510 -2070 0 0 {name=l4 sig_type=std_logic lab=en_1}
C {devices/lab_pin.sym} 1270 -1880 0 1 {name=l24 sig_type=std_logic lab=en_2}
C {xschem_sky130/sky130_stdcells/nand2_1.sym} 1610 -2450 0 0 {name=x61 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 1730 -2450 0 0 {name=x62 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {devices/lab_pin.sym} 1540 -2430 2 1 {name=l45 sig_type=std_logic lab=Ini_en_inv}
C {devices/lab_pin.sym} 1790 -2450 0 1 {name=l58 sig_type=std_logic lab=en_1}
C {devices/lab_pin.sym} 1540 -2470 2 1 {name=l59 sig_type=std_logic lab=clk1ADV}
C {xschem_sky130/sky130_stdcells/nand2_1.sym} 1610 -2350 0 0 {name=x63 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {devices/lab_pin.sym} 1540 -2330 2 1 {name=l60 sig_type=std_logic lab=Ini_en_inv}
C {devices/lab_pin.sym} 1790 -2350 0 1 {name=l61 sig_type=std_logic lab=en_2}
C {devices/lab_pin.sym} 1540 -2370 2 1 {name=l62 sig_type=std_logic lab=clk2ADV}
C {umBMOSC/Non-Overlapping Clock/nonoverlapping_clk_generator.sym} 1540 -1740 0 0 {name=x6 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR}
C {devices/lab_pin.sym} 1690 -1770 0 1 {name=p1 lab=clk1ADV}
C {devices/lab_pin.sym} 1690 -1750 0 1 {name=p3 lab=clk1DEL}
C {devices/lab_pin.sym} 1690 -1730 0 1 {name=p4 lab=clk2DEL}
C {devices/lab_pin.sym} 1690 -1710 0 1 {name=p5 lab=clk2ADV}
C {devices/lab_pin.sym} 1530 -2260 2 1 {name=l65 sig_type=std_logic lab=Ini_en}
C {devices/lab_pin.sym} 1780 -2260 2 0 {name=l66 sig_type=std_logic lab=Ini_en_inv}
C {umBMOSC/analogSwitch/AnSw.sym} 920 -2080 0 0 {name=x1 VGND=VGND VPWR=VPWR}
C {umBMOSC/analogSwitch/AnSw.sym} 1630 -2080 0 0 {name=x2 VGND=VGND VPWR=VPWR}
C {umBMOSC/analogSwitch/AnSw.sym} 1170 -1890 0 1 {name=x5 VGND=VGND VPWR=VPWR}
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 1730 -2350 0 0 {name=x7 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {xschem_sky130/sky130_stdcells/clkinv_1.sym} 1600 -2260 0 0 {name=x8 VGND=VGND VNB=VGND VPB=VPWR VPWR=VPWR prefix=sky130_fd_sc_hd__ }
C {devices/title.sym} 820 -1600 0 0 {name=l1 author="Partha"}
C {devices/opin.sym} 1780 -1990 0 0 {name=p6 lab=VOUT_1}
C {devices/opin.sym} 1060 -2000 0 0 {name=p7 lab=VOUT_2}
C {devices/opin.sym} 1370 -2160 0 0 {name=p8 lab=map1_OUT}
C {devices/opin.sym} 1380 -1970 0 0 {name=p9 lab=map2_OUT}
C {umBMOSC/cMaps/T05.sym} 1190 -2080 0 0 {name=x3 VGND=VGND VPWR=VPWR}
C {umBMOSC/cMaps/T05.sym} 1600 -1890 0 1 {name=x4 VGND=VGND VPWR=VPWR}
C {devices/ipin.sym} 810 -2090 0 0 {name=p12 lab=Ini_con}
C {devices/ipin.sym} 810 -2070 0 0 {name=p13 lab=Ini_en}
C {devices/ipin.sym} 1390 -1770 0 0 {name=p14 lab=ref_clk}
C {devices/ipin.sym} 810 -2010 0 0 {name=p2 lab=VC}
