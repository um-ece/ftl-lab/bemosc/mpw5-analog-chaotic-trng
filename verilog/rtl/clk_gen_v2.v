module clk_gen_v2 (
clk1ADV ,
clk1DEL ,
clk2ADV ,
clk2DEL ,
ref
);
  output clk1ADV ;
  wire clk1ADV ;
  output clk1DEL ;
  wire clk1DEL ;
  output clk2ADV ;
  wire clk2ADV ;
  output clk2DEL ;
  wire clk2DEL ;
  input ref ;
  wire ref ;

wire net6  ;
wire net21  ;
wire net12  ;
wire net7  ;
wire net13  ;
wire net8  ;
wire net14  ;
wire net9  ;
wire net15  ;
wire net16  ;
wire net17  ;
wire net18  ;
wire net19  ;
wire net1  ;
wire net2  ;
wire net3  ;
wire net4  ;
wire net10  ;
wire net5  ;
wire net20  ;
wire net11  ;

sky130_fd_sc_hd__nand2_1 x2 (.A( ref ), .B( net9 ), .VGND( VGND ), .VNB( VGND ), .VPB( VPWR ), .VPWR( VPWR ), .Y( net1 ) ); 
sky130_fd_sc_hd__nand2_1 x8 (.A( net10 ), .B( net11 ), .VGND( VGND ), .VNB( VGND ), .VPB( VPWR ), .VPWR( VPWR ), .Y( net5 ) ); 
sky130_fd_sc_hd__clkinv_1 x1 ( .A( net1 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net2 ) ); 
sky130_fd_sc_hd__clkinv_1 x3 ( .A( net2 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net3 ) ); 
sky130_fd_sc_hd__clkinv_1 x4 ( .A( net3 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( clk1ADV ) ); 
sky130_fd_sc_hd__clkinv_1 x5 ( .A( clk1ADV ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net4 ) ); 
sky130_fd_sc_hd__clkinv_1 x6 ( .A( net4 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( clk1DEL ) ); 
sky130_fd_sc_hd__clkinv_1 x7 ( .A( net5 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net6 ) ); 
sky130_fd_sc_hd__clkinv_1 x9 ( .A( net6 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net7 ) ); 
sky130_fd_sc_hd__clkinv_1 x10 ( .A( net7 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( clk2ADV ) ); 
sky130_fd_sc_hd__clkinv_1 x11 ( .A( clk2ADV ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net8 ) ); 
sky130_fd_sc_hd__clkinv_1 x12 ( .A( net8 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( clk2DEL ) ); 
sky130_fd_sc_hd__clkinv_1 x13 ( .A( ref ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net11 ) ); 
sky130_fd_sc_hd__clkinv_1 x14 ( .A( net12 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net10 ) ); 
sky130_fd_sc_hd__clkinv_1 x15 ( .A( net13 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net12 ) ); 
sky130_fd_sc_hd__clkinv_1 x16 ( .A( net14 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net13 ) ); 
sky130_fd_sc_hd__clkinv_1 x17 ( .A( net15 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net14 ) ); 
sky130_fd_sc_hd__clkinv_1 x18 ( .A( net16 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net15 ) ); 
sky130_fd_sc_hd__clkinv_1 x19 ( .A( net4 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net16 ) ); 
sky130_fd_sc_hd__clkinv_1 x20 ( .A( net17 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net9 ) ); 
sky130_fd_sc_hd__clkinv_1 x21 ( .A( net18 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net17 ) ); 
sky130_fd_sc_hd__clkinv_1 x22 ( .A( net19 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net18 ) ); 
sky130_fd_sc_hd__clkinv_1 x23 ( .A( net20 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net19 ) ); 
sky130_fd_sc_hd__clkinv_1 x24 ( .A( net21 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net20 ) ); 
sky130_fd_sc_hd__clkinv_1 x25 ( .A( net8 ) , .VGND( VGND ) , .VNB( VGND ) , .VPB( VPWR ) , .VPWR( VPWR ) , .Y( net21 ) ); 
endmodule
