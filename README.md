# Caravel Analog User

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) [![CI](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/user_project_ci.yml) [![Caravan Build](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml/badge.svg)](https://github.com/efabless/caravel_user_project_analog/actions/workflows/caravan_build.yml)

---

In this project, we are designing analog discrete-time chaotic oscillator. In stead of using nonlinear mathematical equations, which incur high overhead cost for implementation, we are leveraging the inherent nonlinearity of transistors to come up with novel designs which show good chaotic properties with significantly less overhead cost. In addition, due to inherent analog nature of the core map circuit, this design is susceptible to noise and other PVT variations. Conventional mathematical chaotic maps and their digital implementations are perfectly deterministic. A lot of work has been done to build good quality Random Number Generator (RNG) using these maps. However, they are, by definition, PRNG (Pseudo Random Number Generator). In contrast, our design, due to famous butterfly effect of chaotic systems ampligy slight noise and a RNG developed with analog RNG can be used as a TRNG. Details can be found:
P. S. Paul, M. Sadia, M. R. Hossain, B. Muldrey, and M. S. Hasan, “Design of a Low-Overhead Random Number Generator Using CMOS-based Cascaded Chaotic Maps”, ACM Great Lakes Symposium on VLSI (GLSVLSI), 2021.  

